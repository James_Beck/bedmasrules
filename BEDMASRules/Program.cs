﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEDMASRules
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 23;
            double b = 30;
            double c = 7;
            double d = 15;
            double e = 5;

            ///The equation to test the rules of BEDMAS
            Console.WriteLine("With the variables 23, 30, 7, 15, and 5; the computer will solve (23 + 30) * 7 / (15 - 5)");
            ///The answer here gets rounded
            Console.WriteLine((a + b) * c / (d - e));
            
        }
    }
}
